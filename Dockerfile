#########################
# Create base container #
#########################
FROM ubuntu:24.04 as base
LABEL maintainer="pidydx"

# Base setup
ENV GITLAB_VERSION="17.4.1"
ENV GITLAB_EDITION="ee"
ENV DOWNLOAD_URL="https://packages.gitlab.com/gitlab/gitlab-${GITLAB_EDITION}/packages/ubuntu/noble/gitlab-${GITLAB_EDITION}_${GITLAB_VERSION}-${GITLAB_EDITION}.0_amd64.deb/download.deb"
ENV GITLAB_LOGGER_VERSION="3.0.0"
ENV BASE_PKGS apt-transport-https openssh-server tzdata vim wget perl

# Update users and groups
RUN userdel ubuntu
RUN groupadd -g 992 gitlab-prometheus \
 && groupadd -g 997 registry \
 && groupadd -g 998 git \
 && groupadd -g 999 gitlab-www \
 && useradd -r -m -u 992 -g gitlab-prometheus -m -s /bin/sh -d /var/opt/gitlab/prometheus gitlab-prometheus \
 && useradd -r -m -u 997 -g registry -m -s /bin/sh -d /var/opt/gitlab/gitlab-rails/shared/registry registry \
 && useradd -r -m -u 998 -g git -m -s /bin/sh -d /var/opt/gitlab git \
 && useradd -r -m -u 999 -g gitlab-www -m -s /bin/false -d /var/opt/gitlab/nginx gitlab-www

# Update and install base dependencies
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ca-certificates \
 && apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_PKGS} \
 && rm -rf /var/lib/apt/lists/*

RUN wget -nv ${DOWNLOAD_URL} -O /root/gitlab.deb \
 && dpkg -i /root/gitlab.deb \
 && rm /root/gitlab.deb \
 && echo 'gitlab-docker' > /opt/gitlab/embedded/service/gitlab-rails/INSTALLATION_TYPE \
 && rm -rf /var/lib/apt/lists/*


##########################
# Create build container #
##########################
FROM base AS builder

# Set build dependencies
ENV GO_VERSION="1.19.5"

ENV BUILD_DEPS build-essential git make

# Install build dependencies
RUN apt-get update -q \
&& DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BUILD_DEPS} \
&& rm -rf /var/lib/apt/lists/*

# Run build
WORKDIR /usr/src

RUN wget -nv https://golang.org/dl/go${GO_VERSION}.linux-amd64.tar.gz
RUN tar -xzf go${GO_VERSION}.linux-amd64.tar.gz
ENV PATH=$PATH:/usr/src/go/bin

RUN git clone https://gitlab.com/gitlab-org/cloud-native/gitlab-logger.git --branch v${GITLAB_LOGGER_VERSION}
WORKDIR /usr/src/gitlab-logger
RUN make all
RUN make install


##########################
# Create final container #
##########################
FROM base

# Prepare container
COPY etc/ /etc/
COPY --from=builder /usr/local /usr/local/
COPY usr/ /usr/

ENV LANG=C.UTF-8
ENV TERM xterm
ENV PATH /opt/gitlab/embedded/bin:/opt/gitlab/bin:$PATH

RUN mkdir -p /run/sshd

EXPOSE 443/tcp 8080/tcp 22/tcp
VOLUME ["/etc/gitlab", "/var/opt/gitlab", "/var/log/gitlab"]
HEALTHCHECK --interval=60s --timeout=30s --retries=5 CMD /opt/gitlab/bin/gitlab-healthcheck --fail --max-time 10

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["gitlab"]
