#!/bin/bash

set -e

if [ "$1" = 'init' ]; then
    mkdir -p /var/log/gitlab/reconfigure
    cp /etc/gitlab/init/gitlab.rb /etc/gitlab/gitlab.rb
    cp /etc/gitlab/init/gitlab.root /etc/gitlab/gitlab.root

    if [ ! -d /var/opt/gitlab/ssh ]; then
        mkdir -p /var/opt/gitlab/ssh
        ssh-keygen -f /var/opt/gitlab/ssh/ssh_host_rsa_key -N '' -t rsa
        chmod 0600 /var/opt/gitlab/ssh/ssh_host_rsa_key
        ssh-keygen -f /var/opt/gitlab/ssh/ssh_host_ecdsa_key -N '' -t ecdsa
        chmod 0600 /var/opt/gitlab/ssh/ssh_host_ecdsa_key
        ssh-keygen -f /var/opt/gitlab/ssh/ssh_host_ed25519_key -N '' -t ed25519
        chmod 0600 /var/opt/gitlab/ssh/ssh_host_ed25519_key
    fi

    exec echo "Initialization complete."
fi

if [ "$1" = 'gitlab' ]; then
    /opt/gitlab/embedded/bin/runsvdir-start &
    # If this file and link already exists then gitlab chef will not attempt to run sysctl
    mkdir -p /opt/gitlab/embedded/etc/
    echo "net.core.somaxconn = 1024" > /opt/gitlab/embedded/etc/90-omnibus-gitlab-net.core.somaxconn.conf
    ln -s /opt/gitlab/embedded/etc/90-omnibus-gitlab-net.core.somaxconn.conf /etc/sysctl.d/90-omnibus-gitlab-net.core.somaxconn.conf
    /opt/gitlab/bin/gitlab-ctl reconfigure
    cp /opt/gitlab/embedded/service/gitlab-rails/.gitlab_shell_secret /var/opt/gitlab/gitlab-shell/.gitlab_shell_secret
    cat /etc/gitlab/gitlab.root | gitlab-rake "gitlab:password:reset[root]"
    rm /etc/gitlab/gitlab.root
    exec /usr/local/bin/gitlab-logger --poll=true --exclude="sasl|config|lock|@|gzip|tgz|reconfigure|gz|production.log" --minlevel=3 /var/log/gitlab
fi

if [ "$1" = 'sshd' ]; then
    while [ ! -d /var/opt/gitlab/gitlab-shell ]; do
      echo "Waiting for gitlab to initialize..."
      sleep 1
    done

    mkdir -p /var/log/gitlab/gitlab-shell/
    touch /var/opt/gitlab/gitlab-shell/gitlab-shell.log
    chgrp git /var/opt/gitlab/gitlab-shell/gitlab-shell.log
    chmod g+w /var/opt/gitlab/gitlab-shell/gitlab-shell.log

    ln -s /var/opt/gitlab/gitlab-shell/gitlab-shell.log /var/log/gitlab/gitlab-shell/gitlab-shell.log
    ln -s /var/opt/gitlab/gitlab-shell/config.yml /opt/gitlab/embedded/service/gitlab-shell/config.yml
    ln -s /var/opt/gitlab/gitlab-shell/.gitlab_shell_secret /opt/gitlab/embedded/service/gitlab-shell/.gitlab_shell_secret

    exec /usr/sbin/sshd -D -e -f /etc/gitlab/sshd_config
fi

exec "$@"